// ==UserScript==
// @name         ServiceNow JSON Logger
// @namespace    https://kiririn.io/
// @version      1.0.1
// @description  Add an JSON logger to the ServiceNow script page.
// @author       Dries Meerman <DriesMeerman@Gmail.com>, Tim van Leuverden <TvanLeuverden@Gmail.com>
// @licence	     MIT
// @match        *.service-now.com/sys.scripts.do*
// @grant        none
// @updateURL    https://gitlab.com/Timmy1e/servicenow-json-logger/raw/master/SnowJsonLogger.user.js
// @downloadURL  https://gitlab.com/Timmy1e/servicenow-json-logger/raw/master/SnowJsonLogger.user.js
// ==/UserScript==

(() => {
     const logFunc = "var x = function(a){gs.info(JSON.stringify(a,0,4))};\n\n";
     const code = document.getElementById('runscript');
     if (code) code.innerHTML = logFunc + code.innerHTML;
})();
